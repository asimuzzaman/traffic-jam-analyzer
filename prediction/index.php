<?php
  require_once("../includes/head.php");

  if ($USERNAME == NULL) {
    jump("/sign-in-up?error=notuser");
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Live Traffic</title>

    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
  </head>
  
  <body>
<?php require_once("../includes/header.php"); ?>

    <div class="container" style="margin-top: 100px">
      <form action="" method="post">  
        <div class="row-fluid form-group col-md-6">
          <select name="place" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
<?php
  $query = "SELECT p.place_id, p.name, a.name FROM PLACE p JOIN AREA a ON p.area_id = a.area_id";
  $res = query($query);
  while($out = mysqli_fetch_array($res)){
?>
            <option value="<?php echo $out[0]; ?>" data-subtext="<?php echo $out[2] ?>"><?php echo $out[1] ?></option>
            <!--<option data-subtext="Rep California" disabled="disabled">Marvin Martinez</option>-->
<?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <input class="form-control" type="time" name="usr_time">
          <!--<p><input style="margin-top: 6px;" type="submit"></p>-->
        </div>
        <div class="form-group col-md-6 col-md-offset-3" style="margin-top: 30px;">
          <input class="form-control" type="submit" name="submit" value="Predict">
        </div>
      </form>

<?php
  if (isset($_POST['submit'])) {
?>
      <div style="border: groove; margin-top: 30px;" class="col-md-12 col-md-offset-0">
<?php
    $num_of_jams = 0;

    $place_id = $_POST['place'];

    //calculating the time of the day (current)
    $usr_time = strtotime($_POST['usr_time']);
    $usr_now = $usr_time - strtotime("12:00 am");
    //echo date("h:i d-m-Y ",$usr_now);

    $sum_duration = 0; $counter= 0; //INITIAL VALUE

    $query = "SELECT * FROM JAM WHERE place_id = $place_id";
    $res = query($query);
    while($out = mysqli_fetch_array($res)) {
      $temp = "12:00 am ".date("y-m-d",$out['START_TIME']);
      $start_of_day = strtotime($temp);
      $start_time_jam = $out['START_TIME'] - $start_of_day;

      $end_time_jam = 0; //DEFAULT

      if ($out['END_TIME'] != NULL) {
        $temp = "12:00 am ".date("y-m-d",$out['END_TIME']);
        $start_of_day = strtotime($temp);
        $end_time_jam = $out['END_TIME'] - $start_of_day;
      }
      //echo "<br>".date("h:i d-m-Y",$start_time_jam); //DATA FROM SERVER
      /*echo date("h:i d-m-Y ",$start_time_jam)."<br>";
      echo date("h:i d-m-Y ",($usr_now))."<br>";
      echo date("h:i d-m-Y ",($usr_now+15*60))."<br>";*/

      if ($start_time_jam >= ($usr_now-15*60) && $start_time_jam <= ($usr_now+15*60)) {
        $num_of_jams++;
        
        if($end_time_jam != 0) {
          $sum_duration += $end_time_jam - $start_time_jam;
          $counter++;
        }
      } elseif ($end_time_jam >= ($usr_now-15*60) && $end_time_jam <= ($usr_now+15*60)) {
        $num_of_jams++;

        if($end_time_jam != 0) {
          $sum_duration += $end_time_jam - ($usr_now-15*60);
          $counter++;
        }
      }

    }

    $query = "SELECT name FROM PLACE WHERE place_id = $place_id";
    $res = query($query);
    $out = mysqli_fetch_array($res);
    //echo date("h:i d-m-Y ",$out['START_TIME']);
    
    if ($num_of_jams == 0) {
      echo "<h2>No traffic jam is expected at this time.</h2>";
    } else {
        echo "<h4>Possibility of getting stuck in traffic jam is <strong>";
        
        if ($num_of_jams <= 2) {
          echo "VERY LOW";
        } elseif ($num_of_jams < 5)
          echo "LOW";
      echo "</strong> at this time.</h4>";

    }

    if ($counter !=0) {
      echo "<h3>You may face <strong>".round(($sum_duration/$counter)/60) ."</strong> minutes of traffic jam.</h3>";
      
      $better_time = $usr_time + round($sum_duration/$counter) + 6*60;
      echo "<h4>It would be better if you travel through ".$out[0]." at around <strong>" .date("h:i A.",$better_time)."</strong></h4>";
    }

    echo "</div>";
  }
?>
        <!--<h4>THIS DIV IS FOR YOUR OUTPUT RESULT! THE BORDER HAS BEEN SHOWN FOR YOUR CONVENIENCE. REMOVE THE BORDER WHEN YOU START WORKING! THE HEIGHT WILL STRETCH WITH THE CONTENT.</h4>
        <h4>THIS DIV IS FOR YOUR OUTPUT RESULT! THE BORDER HAS BEEN SHOWN FOR YOUR CONVENIENCE. REMOVE THE BORDER WHEN YOU START WORKING! THE HEIGHT WILL STRETCH WITH THE CONTENT.</h4>
        <h4>THIS DIV IS FOR YOUR OUTPUT RESULT! THE BORDER HAS BEEN SHOWN FOR YOUR CONVENIENCE. REMOVE THE BORDER WHEN YOU START WORKING! THE HEIGHT WILL STRETCH WITH THE CONTENT.</h4>-->
    </div>
  </body>

</html>