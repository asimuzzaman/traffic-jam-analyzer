<?php
  require_once("../includes/head.php");

  if ($USERNAME == NULL) {
    jump("/sign-in-up?error=notuser");
  }

  $success = false;
  if (isset($_POST['submit'])) {
    $place_id = $_POST['place'];

    $query = "INSERT INTO `SUBSCRIBE` VALUES (NULL,$place_id,$USERID)";
    query($query);
    $success = true;
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Subscribe</title>

    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
  </head>
  
  <body>
<?php require_once("../includes/header.php"); ?>

    <div class="container" style="margin-top: 100px">
<?php if($success) { ?>
  <div class="alert alert-success alert-dismissable fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      Succesfully subscribed!
  </div>
<?php } ?>
      <form action="" method="post">  
        <div class="row-fluid form-group col-md-12">
          <select name="place" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
<?php
  $query = "SELECT p.place_id, p.name, a.name FROM PLACE p JOIN AREA a ON p.area_id = a.area_id";
  $res = query($query);
  while($out = mysqli_fetch_array($res)){
?>
            <option value="<?php echo $out[0]; ?>" data-subtext="<?php echo $out[2] ?>"><?php echo $out[1] ?></option>
            <!--<option data-subtext="Rep California" disabled="disabled">Marvin Martinez</option>-->
<?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6 col-md-offset-3" style="margin-top: 30px;">
          <input class="form-control" type="submit" name="submit" value="Subscribe">
        </div>
      </form>
        <!--<h4>THIS DIV IS FOR YOUR OUTPUT RESULT! THE BORDER HAS BEEN SHOWN FOR YOUR CONVENIENCE. REMOVE THE BORDER WHEN YOU START WORKING! THE HEIGHT WILL STRETCH WITH THE CONTENT.</h4>
        <h4>THIS DIV IS FOR YOUR OUTPUT RESULT! THE BORDER HAS BEEN SHOWN FOR YOUR CONVENIENCE. REMOVE THE BORDER WHEN YOU START WORKING! THE HEIGHT WILL STRETCH WITH THE CONTENT.</h4>
        <h4>THIS DIV IS FOR YOUR OUTPUT RESULT! THE BORDER HAS BEEN SHOWN FOR YOUR CONVENIENCE. REMOVE THE BORDER WHEN YOU START WORKING! THE HEIGHT WILL STRETCH WITH THE CONTENT.</h4>-->
    </div>
  </body>

</html>