<?php
  require_once("../../includes/head.php");
  if ($USERID == NULL) {
    jump("/sign-in-up?error=notuser");
  }
  $success = false;

  if (isset($_POST['submit'])) {
    $data = array();

    $data[0] = escape($_POST['fname']);
    $data[1] = escape($_POST['lname']);
    $data[2] = escape($_POST['username']);
    $data[3] = escape($_POST['email']);
    $data[4] = escape($_POST['password']);
    $data[5] = escape($_POST['security_q']);
    $data[6] = escape($_POST['security_a']);

    $query = "UPDATE `USER` SET first_name = '$data[0]', last_name = '$data[1]', email = '$data[3]',";
    $query .= " password = '$data[4]', security_q = '$data[5]', security_a = '$data[6]' WHERE user_id = $USERID";
    query($query);

    $success = true;
  }

  $query = "SELECT * FROM `USER` WHERE USER_ID = $USERID";
  $res = query($query);
  $out = mysqli_fetch_array($res);

  $query = "SELECT COUNT(POST_ID) FROM `POST` WHERE AUTHOR_ID = $USERID";
  $res = query($query);
  $out_2 = mysqli_fetch_array($res);
  //$post_count = $out[0];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Profile</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body>
<?php require_once("../../includes/header.php"); ?>
    <div class="container" style="margin-top: 5%">
      
      <div class="col-md-3 col-lg-3"> <img alt="User Pic" src="/res/avatar-300x300.png" class="img-circle img-responsive" style="margin: 10%">  
        <p> 
          <div class="list-group">
            
            <li class="list-group-item"><label>Coins: </label> <?php echo $out['COIN']; ?><span class="glyphicon glyphicon-bitcoin" style= "float: right;"></span ></li>
            
            <li class="list-group-item"><label>Badges: </label> 9<span class="glyphicon glyphicon-king" style= "float: right;"></span></li>
            
            <li class="list-group-item"><label>Total Traffic Data Input: </label> 27<span class="glyphicon glyphicon-tags" style= "float: right;"></span></li>
            
            <li class="list-group-item"><label>Total post: </label> <?php echo $out_2[0]; ?><span class="glyphicon glyphicon-comment" style= "float: right;"></span></li>
          </div>
        </p>
      </div>

      <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
<?php if($success) { ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> Changes have been updated.
        </div>      
<?php } ?>
        <form class="form-horizontal" action="/profile/edit/index.php" method="post">
          <div>     
                <div class="form-group">
                  <label for="fname">First Name:</label>
                  
                    <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $out['FIRST_NAME']; ?>">
                  
                </div>

                <div class="form-group">
                  <label for="lname">Last Name:</label>
                  
                    <input type="text" class="form-control" name="lname" id="lname" value="<?php echo $out['LAST_NAME']; ?>">
                  
                </div>

                <div class="form-group">
                  <label for="username">Username:</label>
                  
                    <input type="text" class="form-control" id="username" placeholder="<?php echo $out['USERNAME']; ?>" disabled>
                
                
                </div>

                <div class="form-group">
                  <label for="email">Email:</label>
                  
                    <input type="email" class="form-control" name="email" id="email" value=" <?php echo $out['EMAIL']; ?> ">
                  
                </div>

                <div class="form-group">
                  <label for="pwd">Password:</label>
                
                    <input type="password" class="form-control" name="password" id="pwd" value="<?php echo $out['PASSWORD']; ?>">
                
                </div>

                <div class="form-group">
                  <label for="confpwd">Confirm Password:</label>
                
                    <input type="password" class="form-control" id="confpwd" placeholder="Confirm password">
                  
                </div>

                <div class="form-group">
                  <label for="ques">Security Question:</label>
                  
                    <input type="text" class="form-control" name="security_q" id="ques" value=" <?php echo $out['SECURITY_Q']; ?> ">
                  
                </div>

                <div class="form-group">
                  <label for="ans">Answer:</label>
                  
                    <input type="text" class="form-control" name="security_a" id="ans" value=" <?php echo $out['SECURITY_A']; ?> ">
                  
                </div>

                <div class="form-group">
                  
                    <input type="submit" name="submit" class="btn btn-primary btn-lg col-lg-4 col-md-5 col-sm-2 col-lg-offset-0 col-md-offset-0 col-sm-offset-0" value="Save Changes">
                    <a href="/" class="btn btn-info btn-lg col-lg-3 col-md-6 col-sm-2 col-lg-offset-5 col-md-offset-1 col-sm-offset-5">Cancel</a>
                    <!-- col-lg-4 means button size and col-lg-offset-2 means starting from col 2 in the div-->
                    
                </div>
          </div>   
        </form>
      </div>


    </div>

  </body>

</html>  