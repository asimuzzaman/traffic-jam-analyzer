<?php
  require_once("../includes/head.php");

  $data = array();
  $data[0] = NULL;
  $data[1] = NULL;
  $data[2] = NULL;
  $data[3] = NULL;
  $title = "Not found!";

  if (isset($_GET['uid'])) {
    $uid = $_GET['uid'];

    $query = "SELECT last_name, username, email, coin FROM `USER` WHERE USER_ID = $uid";
    $res = query($query);
    $out = mysqli_fetch_array($res);

    $data[0] = $out['username'];
    $data[1] = $out['email'];
    $data[2] = $out['coin'];
    $title = $out['last_name'] . "'s Profile";

    $query = "SELECT COUNT(POST_ID) FROM `POST` WHERE AUTHOR_ID = $uid";
    $res = query($query);
    $out = mysqli_fetch_array($res);
    $data[3] = $out[0];
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body>
<?php require_once("../includes/header.php"); ?>

    <div class="container" style="margin-top: 5%">
      <div class="col-md-3 col-lg-3"> <img alt="User Pic" src="/res/avatar-300x300.png" class="img-circle img-responsive" style="margin: 10%"> 
      </div>
      
      <div class="panel default" style="margin: 5%">
            
        <h1 align="center">@<?php echo $data[0]; ?></h1>
        <hr>
        <div class="panel-body">
          
          <div class="row">
            
            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 col-xs-offset-0 col-sm-offset-2 col-md-offset-1 col-lg-offset-2"> 
              <div class="list-group">
                <li class="list-group-item"><label>Email: </label> <?php echo $data[1]; ?><span class="glyphicon glyphicon-envelope" style= "float: right;"></span></li> 
                
                <li class="list-group-item"><label>Coins: </label> <?php echo $data[2]; ?><span class="glyphicon glyphicon-bitcoin" style= "float: right;"></span ></li>
                
                <li class="list-group-item"><label>Badges: </label> 9<span class="glyphicon glyphicon-king" style= "float: right;"></span></li>
                
                <li class="list-group-item"><label>Total Traffic Data Input: </label> 27<span class="glyphicon glyphicon-tags" style= "float: right;"></span></li>
                
                <li class="list-group-item"><label>Total post: </label> <?php echo $data[3]; ?><span class="glyphicon glyphicon-comment" style= "float: right;"></span></li>
              </div>
              
            </div>
          </div>         
          
        </div>
          
      </div> <!-- Panel ends here -->

    </div>

  </body>

</html>  