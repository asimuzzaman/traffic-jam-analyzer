<?php

  function ConnectDb() {
    global $connect;
    $connect = mysqli_connect("localhost","root","4321","traffic_jam");

    if(mysqli_connect_errno()) {
      echo "Unable to connect to database: " . mysqli_connect_error() ."<br/>";
    }
  }

  function query($query) {
    global $connect;
    $result = mysqli_query($connect,$query) or die(mysqli_error($connect));
    return $result;
  }

  //returns primary key of last INSERT in case of Auto increment
  function GetDbId() {
    global $connect;
    return mysqli_insert_id($connect);
  }

  function escape($str) {
    global $connect;
    return mysqli_real_escape_string($connect,$str);
  }

  function CloseDb() {
    global $connect;
    mysqli_close($connect);
  }

	header('Content-Type: text/xml');
	$output = "<?xml version=\"1.0\"?>\n";
	echo $output;
	
	ConnectDb();
	/*echo "<data>";
	echo "<reported>up</reported>";
	echo "<count>400</count>";
	echo "</data>";*/
	if (isset($_GET['key'])) {
		$keyword = $_GET['key'];

		echo "<result>";
		$query = "SELECT * FROM SEARCH WHERE title LIKE '%$keyword%'";
		$res = query($query);
		while($out = mysqli_fetch_array($res)) {
			echo "<content>". $out['TITLE']. "</content>";
		}
		echo "</result>";

	}
?>