<?php
  require_once('../includes/head.php');
  if($USERID == NULL) jump("/sign-in-up?error=notuser");

  $success = false;
  $search = "";

  if (isset($_POST['new_post'])) {
    $data = array();
    $data[0] = escape($_POST['title']);
    $data[1] = escape($_POST['body']);
    $tags = $_POST['tags'];
    $time = time();
    $type = $_POST['type'];
    /**
    Note = 1
    Question = 2
    **/

    $query = "INSERT INTO `POST` VALUES (NULL, '$data[0]',0,$USERID,0,'$data[1]',$time,$type);";
    query($query);

    $post_id = GetDbId();
    for ($i=0; $i < count($tags) ; $i++) { 
      $query = "INSERT INTO `LOCATION_TAG` VALUES (NULL,$tags[$i],$post_id);";
      query($query); 
    }
    $success = true;
  }

  if (isset($_POST['search'])) {
    $search = $_POST['keyword'];
  }
  //last location of current user
  $query = "SELECT last_location FROM `USER` WHERE user_id = $USERID";
  $res = query($query);
  $out = mysqli_fetch_array($res);
  $last_location = $out['last_location'];

  $yesterday = strtotime('-24 hours'); //for detecting post of yesterday (24 hours from now)

  function is_listed($list, $post_id) { //searches for already non-listed posts
    $listed = false;
    for($i=0; $i < count($list);$i++) {
      if($list[$i] == $post_id) {
        $listed = true;
        break;
      }
    }
    return $listed;
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Forum View</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body onload="setNavbar('forum')">
<?php require_once("../includes/header.php"); ?>

    <div class="container" style="margin-top: 35px">
      <div class="page-header page-heading"> <!-- STUPID DON'T FORGET TO CHANGE THIS FOR OTHER PAGES!-->
      <div class="row">
        <h1 class="col-lg-40 col-lg-offset-0 col-md-40 col-lg-offset-0 col-xs-40 col-xs-offset-1">Forums <span class=" glyphicon glyphicon-link"></span></h1>
        <h4 class="col-lg-40 col-lg-offset-0 col-xs-40 col-xs-offset-1">Please follow the forum rules and always check the recent posts before posting to prevent duplicate posts.</h4>
        
      </div>
    </div>  

    <div class="row">
      <button type="button" class="btn btn-danger btn-lg col-lg-2 col-lg-offset-0 col-md-2 col-md-offset-0 col-sm-5 col-sm-offset-0 col-xs-10 col-xs-offset-1" style="margin-bottom: 6%"
      data-toggle="modal" data-target="#myModal">Create New Post</button> 
                                <!--MODAL HERE! -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
    
                                <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Post</h4>
            </div>
            <div class="modal-body">
              <form action="/community/index.php" method="post">
                <div class="form-group">
                  <label for="title">Title:</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Enter your post title">
                </div>
                <div class="form-group">
                  <label for="type">Post as: </label>
                  <label class="radio-inline">
                    <input type="radio" id="type" name="type" value="1" checked="checked">Note
                  </label>
                  <label class="radio-inline">
                    <input type="radio" id="type" name="type" value="2">Question
                  </label>
                </div>
                <div class="form-group">
                  <label for="body">Description:</label>
                  <textarea class="form-control" name="body" rows="5" id="body" placeholder="Description here"></textarea>
                </div>
                <div class="form-group">
                  <label for="location">Tag location:</label>
                  <select size="3" name="tags[]" class="selectpicker form-control" multiple>
<?php
  $query = "SELECT * FROM AREA"; //city is not specified yet
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
?>
                        <option value="<?php echo $out['AREA_ID']; ?>"><?php echo $out['NAME']; ?></option>
<?php
  }
?>                                        
                  </select>
                </div>
                <div class="modal-footer">
                  <input type="submit" name="new_post" class="btn btn-primary" value="Submit">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </form>  
            </div>       

          </div>
        
      </div> 
    </div>  

    <form class="col-md-3 col-md-offset-7 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-1" style="margin-bottom: 10px;" action="/community/index.php" method="post">
      <div class="input-group">
        <input type="text" name="keyword" class="search-query form-control" placeholder="Search" />
          <span class="input-group-btn">
            <button class="btn btn-danger" type="submit" name="search">
              <span class=" glyphicon glyphicon-search"></span>
            </button>
          </span>
      </div>  
    </form>  
                               <!-- TAB STARTS FROM HEREEEEEEE -->
    <div style= "margin-top: 20px" class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0 col-sm-20 col-sm-offset-0 col-sm-20 col-sm-offset-0">

<?php if($success) { ?>
  <div class="alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Post added successfully!
  </div>
<?php } ?>       
      <ul class="nav nav-tabs nav-justified col-lg-12 col-sm-30 col-sm-offset-0"> 
          <li class="active" style="display:table-cell !important;"><a data-toggle="tab" href="#notes"><h2>Notes <span class="glyphicon glyphicon-pushpin"></span></h2></a></li>
          
          <li style="display:table-cell !important;"><a data-toggle="tab" href="#questions"><h2>Questions <span class="glyphicon glyphicon-question-sign"></span></h2></a></li>
      </ul>

      
    <div class="tab-content">
        
      <div id="notes" class="tab-pane fade in active">

          <table class="table forum table-striped">
            <thead>
              <tr>
                <th>
                  <h2></h2> <!-- don't change this line-->
                </th>
              </tr>
            </thead>
            <tbody>
<?php
  //implementation of location tag based sorting
  $counter = 0;
  $already_listed = array();

  $query = "SELECT p.post_id, p.title, p.body, u.username, p.time, p.vote, p.author_id, l.area_id FROM `USER` u JOIN POST p ON u.user_id = p.author_id";
  $query .= " JOIN LOCATION_TAG l ON p.post_id = l.post_id WHERE p.type = 1 AND (p.title LIKE '%$search%' OR p.body LIKE '%$search%') ORDER BY p.time DESC, u.coin DESC";
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
    
    if(($out['time'] >= $yesterday) && $last_location == $out['area_id']) {
      $body = substr($out['body'],0,100);
      if(strlen($body) > 100) $body .= "......";

      $already_listed[$counter++] = $out['post_id']; //remembering already listed posts for future use 
      $post_id = $out['post_id'];
      $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
      $res1 = query($query);
      $out1 = mysqli_fetch_array($res1);
?>           
            <tr>
          <td>
            <div class="media">
              <a class="pull-left" href="#">
              <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="70" height="70">
            </a>

            <div class="media-body">
              <div class="row"> 
                <a href="/community/post?pid=<?php echo $out['post_id']; ?>">
                  <h4 class="media-heading col-lg-10"><?php echo $out['title']; ?></h4>
                </a>
                
             </div> 

              <p><?php echo $body; ?></p>

              <ul class="list-inline list-unstyled">
                <li>
                  By<a href="/profile?uid=<?php echo $out['author_id']; ?>"><i> <?php echo $out['username']; ?></i></a>
                </li>
                <li>|</li>
                <li>
                  <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['time']); ?> </span>
                </li>
                <li>|</li>
                  <span><i class="glyphicon glyphicon-comment"></i> <?php echo $out1[0]; ?> comments</span>
                <li>|</li>
                <li>
                   <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $out['vote']; ?> Votes</span>
                </li>
              </ul>
            </div>
          </div>
          </td>
        </tr>
<?php   }
      } 
  $no_post = true;

  $query = "SELECT p.post_id, p.title, p.body, u.username, p.time, p.vote, p.author_id FROM `USER` u JOIN POST p ON u.user_id = p.author_id";
  $query .= " WHERE p.type = 1 AND (p.title LIKE '%$search%' OR p.body LIKE '%$search%') ORDER BY p.time DESC, u.coin DESC";
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
    $body = substr($out['body'],0,100);
    if(strlen($body) > 100) $body .= "......";
    $no_post = false;

    if(!is_listed($already_listed,$out['post_id'])) {
      $post_id = $out['post_id'];
      $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
      $res1 = query($query);
      $out1 = mysqli_fetch_array($res1);
?>           
            <tr>
          <td>
            <div class="media">
              <a class="pull-left" href="#">
              <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="70" height="70">
            </a>

            <div class="media-body">
              <div class="row"> 
                <a href="/community/post?pid=<?php echo $out['post_id']; ?>">
                  <h4 class="media-heading col-lg-10"><?php echo $out['title']; ?></h4>
                </a>
                
             </div> 

              <p><?php echo $body; ?></p>

              <ul class="list-inline list-unstyled">
                <li>
                  By<a href="/profile?uid=<?php echo $out['author_id']; ?>"><i> <?php echo $out['username']; ?></i></a>
                </li>
                <li>|</li>
                <li>
                  <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['time']); ?> </span>
                </li>
                <li>|</li>
                  <span><i class="glyphicon glyphicon-comment"></i> <?php echo $out1[0]; ?> comments</span>
                <li>|</li>
                <li>
                   <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $out['vote']; ?> Votes</span>
                </li>
              </ul>
            </div>
          </div>
          </td>
        </tr>
<?php } } ?>
      </tbody>
    </table>

<?php
  if ($no_post) {
    # code...
    echo "<h3 align=\"center\">Nothing to show.</h1>";
  }
?>
    </div>
<!--QUESTION SECTION STARTS HERE-->
      <div id="questions" class="tab-pane fade">

        <table class="table forum table-striped">
      <thead>
        <tr>

          <th>
            <h2></h2>
          </th>
        </tr>
      </thead>
      <tbody>
<?php
  //implementation of location tag based sorting
  $counter = 0;
  $already_listed = array();

  $query = "SELECT p.post_id, p.title, p.body, u.username, p.time, p.vote, p.author_id, l.area_id FROM `USER` u JOIN POST p ON u.user_id = p.author_id";
  $query .= " JOIN LOCATION_TAG l ON p.post_id = l.post_id WHERE p.type = 2 AND (p.title LIKE '%$search%' OR p.body LIKE '%$search%') ORDER BY p.time DESC, u.coin DESC";
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
    
    if(($out['time'] >= $yesterday) && $last_location == $out['area_id']) {
      $body = substr($out['body'],0,100);
      if(strlen($body) > 100) $body .= "......";

      $already_listed[$counter++] = $out['post_id']; //remembering already listed posts for future use
      $post_id = $out['post_id'];
      $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
      $res1 = query($query);
      $out1 = mysqli_fetch_array($res1);
?>           
            <tr>
          <td>
            <div class="media">
              <a class="pull-left" href="#">
              <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="70" height="70">
            </a>

            <div class="media-body">
              <div class="row"> 
                <a href="/community/post?pid=<?php echo $out['post_id']; ?>">
                  <h4 class="media-heading col-lg-10"><?php echo $out['title']; ?></h4>
                </a>
                
             </div> 

              <p><?php echo $body; ?></p>

              <ul class="list-inline list-unstyled">
                <li>
                  By<a href="/profile?uid=<?php echo $out['author_id']; ?>"><i> <?php echo $out['username']; ?></i></a>
                </li>
                <li>|</li>
                <li>
                  <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['time']); ?> </span>
                </li>
                <li>|</li>
                  <span><i class="glyphicon glyphicon-comment"></i> <?php echo $out1[0]; ?> comments</span>
                <li>|</li>
                <li>
                   <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $out['vote']; ?> Votes</span>
                </li>
              </ul>
            </div>
          </div>
          </td>
        </tr>
<?php   }
      } 
  $no_post = true;

  $query = "SELECT p.post_id, p.title, p.body, u.username, p.time, p.vote, p.author_id FROM `USER` u JOIN POST p ON u.user_id = p.author_id";
  $query .= " WHERE p.type = 2 AND (p.title LIKE '%$search%' OR p.body LIKE '%$search%') ORDER BY p.time DESC, u.coin DESC";
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
    $body = substr($out['body'],0,100);
    if(strlen($body) > 100) $body .= "......";
    $no_post = false;

    if(!is_listed($already_listed,$out['post_id'])) {
      $post_id = $out['post_id'];
      $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
      $res1 = query($query);
      $out1 = mysqli_fetch_array($res1);
?>           
            <tr>
          <td>
            <div class="media">
              <a class="pull-left" href="#">
              <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="70" height="70">
            </a>

            <div class="media-body">
              <div class="row"> 
                <a href="/community/post?pid=<?php echo $out['post_id']; ?>">
                  <h4 class="media-heading col-lg-10"><?php echo $out['title']; ?></h4>
                </a>
                
             </div> 

              <p><?php echo $body; ?></p>

              <ul class="list-inline list-unstyled">
                <li>
                  By<a href="/profile?uid=<?php echo $out['author_id']; ?>"><i> <?php echo $out['username']; ?></i></a>
                </li>
                <li>|</li>
                <li>
                  <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['time']); ?> </span>
                </li>
                <li>|</li>
                  <span><i class="glyphicon glyphicon-comment"></i> <?php echo $out1[0]; ?> comments</span>
                <li>|</li>
                <li>
                   <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $out['vote']; ?> Votes</span>
                </li>
              </ul>
            </div>
          </div>
          </td>
        </tr>
<?php } } ?>
      </tbody>
    </table>

<?php
  if ($no_post) {
    # code...
    echo "<h3 align=\"center\">Nothing to show.</h1>";
  }
?>
      </div>
    </div>
  </div>      
</div>
</div>

  </body>

</html>  