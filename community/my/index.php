<?php
  require_once("../../includes/head.php");
  if($USERID == NULL) jump("/sign-in-up?error=notuser");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My Posts</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body>
<?php require_once("../../includes/header.php"); ?>

    <div class="container center-div" style="margin-top:150px">
      
      <div class="row">
                            <!-- TAB STARTS FROM HEREEEEEEE -->
      <div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0 col-sm-20 col-sm-offset-0 col-sm-20 col-sm-offset-0">
      
      <ul class="nav nav-tabs nav-justified col-lg-12 col-sm-30 col-sm-offset-0"> 
          <li class="active" style="display:table-cell !important;"><a data-toggle="tab" href="#mynotes"><h2>My Notes <span class="glyphicon glyphicon-pushpin"></span></h2></a></li>
          
          <li style="display:table-cell !important;"><a data-toggle="tab" href="#myquestions"><h2>My Questions <span class="glyphicon glyphicon-question-sign"></span></h2></a></li>
      </ul>

      
    <div class="tab-content">
        
      <div id="mynotes" class="tab-pane fade in active">

          <table class="table forum table-striped">
            <thead>
              <tr>
                <th>
                  <h2></h2> <!-- don't change this line-->
                </th>
              </tr>
            </thead>
            <tbody>
<?php
  $no_post = true;

  $query = "SELECT * FROM `POST` WHERE author_id = $USERID AND type = 1 ORDER BY time DESC;";
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
    $body = substr($out['BODY'],0,100);
    if(strlen($body) > 100) $body .= "......";
    $no_post = false;
    $post_id = $out['POST_ID'];
    $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
    $res1 = query($query);
    $out1 = mysqli_fetch_array($res1);
?>
              <tr>
                <td>
                  <div class="media">
                    <a class="pull-left" href="#">
                      <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="70" height="70">
                    </a>

                    <div class="media-body">
                      <div class="row"> 
                        <a href="/community/post?pid=<?php echo $out['POST_ID']; ?>"><h4 class="media-heading col-lg-10"><?php echo $out['TITLE']; ?></h4></a>
                      </div> 

                      <p><?php echo $body; ?></p>

                      <ul class="list-inline list-unstyled">
                          <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['TIME']); ?> </span>
                        </li>
                        <li>|</li>
                          <span><i class="glyphicon glyphicon-comment"></i> <?php echo $out1[0]; ?> comments</span>
                        <li>|</li>
                        <li>
                           <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $out['VOTE']." votes"; ?></span>
                        </li>
                      </ul>
                  </div>
                </div>
              </td>
            </tr>
<?php } ?>

      </tbody>
    </table>
<?php
  if ($no_post) {
    # code...
    echo "<h3 align=\"center\">Nothing to show.</h1>";
  }
?>
    </div>

      <div id="myquestions" class="tab-pane fade">

        <table class="table forum table-striped">
      <thead>
        <tr>

          <th>
            <h2></h2>
          </th>
        </tr>
      </thead>
      <tbody>
<?php
  $no_post = true;

  $query = "SELECT * FROM `POST` WHERE author_id = $USERID AND type = 2 ORDER BY time DESC;";
  $res = query($query);
  while ($out = mysqli_fetch_array($res)) {
    $body = substr($out['BODY'],0,100);
    if(strlen($body) > 100) $body .= "......";
    $no_post = false;
    $post_id = $out['POST_ID'];
    $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
    $res1 = query($query);
    $out1 = mysqli_fetch_array($res1);
?>
              <tr>
                <td>
                  <div class="media">
                    <a class="pull-left" href="#">
                      <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="70" height="70">
                    </a>

                    <div class="media-body">
                      <div class="row"> 
                        <a href="/community/post?pid=<?php echo $out['POST_ID']; ?>"><h4 class="media-heading col-lg-10"><?php echo $out['TITLE']; ?></h4></a>
                      </div> 

                      <p><?php echo $body; ?></p>

                      <ul class="list-inline list-unstyled">
                          <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['TIME']); ?> </span>
                        </li>
                        <li>|</li>
                          <span><i class="glyphicon glyphicon-comment"></i> <?php echo $out1[0]; ?> comments</span>
                        <li>|</li>
                        <li>
                           <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $out['VOTE']." votes"; ?></span>
                        </li>
                      </ul>
                  </div>
                </div>
              </td>
            </tr>
<?php } ?>
      </tbody>
      </table>
<?php
  if ($no_post) {
    # code...
    echo "<h3 align=\"center\">Nothing to show.</h1>";
  }
?>
      </div>
    </div>
  </div> 
 </div>
 </div>      


  </body>

</html>  